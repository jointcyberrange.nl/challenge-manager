import {css, html, LitElement} from 'https://unpkg.com/lit-element/lit-element.js?module';

class ApplicationComponent extends LitElement {
    // BASE_URL = "https://challenge-manager-jcr.herokuapp.com"
    BASE_URL = "http://localhost:8080"
    previouslyUploaded = [];
    myUploadedFile;

    static get properties() {
        return {
            challengeList: {type: JSON}, filteredList: {type: JSON}, exportChallengeList: {type: JSON}
        }
    }

    static get styles() {
        return css`
            .menu {
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
            }

            #overwriteDiv label {
                display: inline;
            }

            #upload-button {
                cursor: pointer;
                border-radius: 0.75rem;
                --tw-bg-opacity: 1;
                background-color: indianred;
                padding: 0.875rem 2rem 0.875rem 2rem;
                font-family: Conthrax, sans-serif, Arial;
                font-size: 16px;
                --tw-text-opacity: 1;
                color: indianred;
                border: 0;
                margin-left: 0.75rem;
            }

            #export-button {
                cursor: pointer;
                border-radius: 0.75rem;
                --tw-bg-opacity: 1;
                background-color: rgba(237, 37, 78, 100);
                padding: 0.875rem 2rem 0.875rem 2rem;
                font-family: Conthrax, sans-serif, Arial;
                font-size: 16px;
                --tw-text-opacity: 1;
                color: rgba(255, 255, 255, 100);
                border: 0;
                margin-left: 0.75rem;
            }

            input[type="file"] {
                display: none;
            }

            .custom-file-upload {
                cursor: pointer;
                border-radius: 0.75rem;
                --tw-bg-opacity: 1;
                background-color: rgba(237, 37, 78, 100);
                padding: 0.875rem 2rem 0.875rem 2rem;
                font-family: Conthrax, sans-serif, Arial;
                font-size: 16px;
            }

            .custom-file-upload:hover {
                background-color: rgba(168, 26, 54, 100);
            }

            #export-button:hover {
                background-color: rgba(168, 26, 54, 100);
            }

            #search-form input {
                cursor: pointer;
                border-radius: 0.75rem;
                --tw-bg-opacity: 1;
                padding: 0.875rem 2rem 0.875rem 2rem;
                font-family: Conthrax, sans-serif, Arial;
                font-size: 16px;
                border: 3px solid rgba(237, 37, 78, 100);
                margin-bottom: 0.5rem;
                color: black;
            }

            #overwriteDiv {
                margin-bottom: 0.5rem;
                margin-top: 0.2rem;
                text-align: left;
            }

            .search-div {
                background-color: #01102c;
                padding: 1rem 2rem 0.5rem;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-top: 1rem;
                display: none;
            }

            .check-all-div {
                left: 14rem;
                position: absolute;
                top: 13.5rem;
                display: none;
            }

            #clearSessionButton {
                position: absolute;
                cursor: pointer;
                border-radius: 0.5rem;
                --tw-bg-opacity: 1;
                background-color: white;
                padding: 0.2rem 1rem 0.2rem 1rem;
                font-family: Futura Std Medium, serif;
                font-size: 16px;
                --tw-text-opacity: 1;
                color: #01102c;
                border: 0;
                right: 5px;
            }

            #clearSessionButton:hover {
                background-color: rgba(191, 191, 191, 100);
            }

            .lds-dual-ring {
                display: none;
                position: absolute;
                top: 15rem;
            }

            .lds-dual-ring:after {
                content: " ";
                display: block;
                width: 10em;
                height: 10em;
                margin: 1em;
                border-radius: 50%;
                border: 1em solid;
                border-color: rgba(237, 37, 78, 100) transparent rgba(237, 37, 78, 100) transparent;
                animation: lds-dual-ring 1.2s linear infinite;
            }

            .error-div {
                display: none;
                margin-top: 1rem;
            }

            previous-files {
                display: none;
            }

            @keyframes lds-dual-ring {
                0% {
                    transform: rotate(0deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }
        `;
    }

    render() {
        return html`
            <button id="clearSessionButton">Clear session</button>
            <div class="menu">
                <div>

                    <div id="overwriteDiv">
                        <input type="checkbox" id="overwriteCheckBox" name="Overwrite">
                        <label for="overwriteCheckBox">Overwrite previous challenges</label><br>

                        <input type="checkbox" id="removeUserDataCheckBox" name="Overwrite">
                        <label for="removeUserDataCheckBox">Remove personal data</label><br>
                    </div>
                    <label for="uploadFile" class="custom-file-upload">
                        <i class="fa fa-cloud-upload"></i> Import
                    </label>
                    <input type="file" id="uploadFile">

                    <button id="export-button"> Export</button>
                </div>
                <div class="error-div"></div>

                <div class="lds-dual-ring"></div>

                <div class="check-all-div">
                    <input type="checkbox" id="select-all-button">
                    <label for="select-all-button">Select all</label>
                </div>
                <div class="search-div">
                    <form id="search-form">
                        <div class="topnav">
                            <input id="challengeFilter" type="text" placeholder="Search..">
                        </div>
                    </form>
                </div>
            </div>

            <data-view .filteredList="${this.filteredList}"
                       .exportChallengeList="${this.exportChallengeList}"></data-view>
            <previous-files .previouslyUploaded="${this.previouslyUploaded}"></previous-files>
        `;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
        let searchBar = this.shadowRoot.querySelector(".search-div");
        let checkAllDiv = this.shadowRoot.querySelector(".check-all-div");
        let selectAll = this.shadowRoot.querySelector("#select-all-button");
        let error = this.shadowRoot.querySelector(".error-div");

        this.exportChallengeList = []

        let uploadButton = this.shadowRoot.querySelector("#uploadFile");
        uploadButton.onchange = (event) => {
            this._uploadFile();
            event.target.value = '';
        }

        this.shadowRoot.addEventListener("isCheckedEvent", (event) => {
            this.exportChallengeList.push(event.detail)
        })

        this.shadowRoot.addEventListener("isUncheckedEvent", (event) => {
            for (let i = 0; i < this.exportChallengeList.length; i++) {
                if (this.exportChallengeList[i] === event.detail) {
                    this.exportChallengeList.splice(i, 1)
                }
            }
            selectAll.checked = false;
        })

        this.shadowRoot.querySelector("#export-button").addEventListener("click", () => {
            if (this.myUploadedFile === undefined) {
                error.style.display = "block";
                error.style.color = "red"
                error.textContent = "Please import a file";
                setTimeout(() => {
                    error.style.display = "none";
                }, 5000)

            } else {
                this._exportChallenges(this.exportChallengeList)
            }
        })


        selectAll.addEventListener("click", () => {
            let allChallenges = this.shadowRoot.querySelector("data-view").shadowRoot.querySelector(".data-view-container").children;
            for (const challenge of allChallenges) {
                let checkboxOfChallenge = challenge.shadowRoot.querySelector(".challengeCheckboxInput")
                if (selectAll.checked) {
                    checkboxOfChallenge.checked = true;
                    this.exportChallengeList = [...this.challengeList];
                } else {
                    checkboxOfChallenge.checked = false;
                    this.exportChallengeList = [];
                }
            }
        })

        let clearSessionButton = this.shadowRoot.querySelector("#clearSessionButton");

        clearSessionButton.addEventListener("click", () => {
            this.filteredList = [];
            this.previouslyUploaded = [];
            this.shadowRoot.querySelector("data-view").style.display = "none";
            this.shadowRoot.querySelector("previous-files").style.display = "none";
            searchBar.style.display = "none";
            checkAllDiv.style.display = "none";
            this.exportChallengeList = [];
            this.challengeList = [];
            clearSessionButton.textContent = "Cleared session!"
            clearSessionButton.disabled = true;
            setTimeout(() => {
                clearSessionButton.textContent = "Clear session"
                clearSessionButton.disabled = false;
            }, 3000)
        });

        this.shadowRoot.querySelector("#challengeFilter").addEventListener("keydown", function (key) {
            if (key.keyCode === 13) {
                key.preventDefault();
            }
        })

        this.shadowRoot.querySelector("#challengeFilter").addEventListener("keyup", () => {
            this._filterResultsChallengeList(this.shadowRoot.querySelector("#challengeFilter").value);
        })
    }

    _uploadFile() {
        let importInput = this.shadowRoot.querySelector("#uploadFile");
        let errorDiv = this.shadowRoot.querySelector(".error-div");
        this.myUploadedFile = importInput.files[0];
        if (this.myUploadedFile === undefined) return;

        let selectAll = this.shadowRoot.querySelector("#select-all-button");

        let previousFilesComponent = this.shadowRoot.querySelector("previous-files");
        let formData = new FormData();
        formData.append("file", this.myUploadedFile);

        let loadingIcon = this.shadowRoot.querySelector(".lds-dual-ring");
        loadingIcon.style.display = "inline-block";

        fetch(`${(this.BASE_URL)}/ctfdexport/upload`, {
            method: "POST", body: formData
        }).then(response => {
            if (response.ok) {
                this.shadowRoot.querySelector(".search-div").style.display = "block";
                this.shadowRoot.querySelector(".check-all-div").style.display = "block";
                this.shadowRoot.querySelector("data-view").style.display = "block";
                this.shadowRoot.querySelector("previous-files").style.display = "block";
                return response.json()
            } else {
                throw new Error("Corrupted file")
            }
        })
            .then(data => {
                let overwriteChecked = this.shadowRoot.querySelector("#overwriteCheckBox").checked

                if (overwriteChecked || this.challengeList === undefined) {
                    // disable all checkboxes of challenges when a new file is uploaded
                    if (this.shadowRoot.querySelector("data-view").shadowRoot.querySelector(".data-view-container") !== null) {
                        let allChallenges = this.shadowRoot.querySelector("data-view").shadowRoot.querySelector(".data-view-container").children;
                        for (const challenge of allChallenges) {
                            let checkboxOfChallenge = challenge.shadowRoot.querySelector(".challengeCheckboxInput")
                            checkboxOfChallenge.checked = false;
                        }
                    }
                    this.previouslyUploaded = [];
                    selectAll.checked = false;
                    this.exportChallengeList = [];
                    this.challengeList = data;
                } else {
                    this.challengeList = this.challengeList.concat(data)
                }
                this.filteredList = this.challengeList;
            }).then(() => {
            this.previouslyUploaded.push(this.myUploadedFile.name);
            previousFilesComponent.updateList();
            importInput.value = "";
            loadingIcon.style.display = "none";
        })
            .catch((error) => {
                loadingIcon.style.display = "none";
                errorDiv.style.display = "block";
                errorDiv.style.color = "red";
                errorDiv.textContent = "Corrupt file. Please try importing another one"
                setTimeout(() => {
                    errorDiv.style.display = "none"
                }, 5000)
                console.log(error)
            });
    }

    _exportChallenges(challenges) {
        let formData = new FormData();
        formData.append("file", this.myUploadedFile);
        formData.append("jsonChallenges", JSON.stringify({count: challenges.length, results: challenges}));
        formData.append("deletePersonalData", this.shadowRoot.querySelector("#removeUserDataCheckBox").checked);
        if (this.exportChallengeList.length !== 0) {
            let loadingIcon = this.shadowRoot.querySelector(".lds-dual-ring");
            loadingIcon.style.display = "inline-block";

            fetch(`${(this.BASE_URL)}/ctfdexport/export`, {
                method: "POST", body: formData
            }).then((res) => {
                if (res.ok) {
                    return res.blob();
                } else {
                    return Promise.reject
                }
            }).then((data) => {
                let download = document.createElement("a");
                let date_today = new Date().toLocaleDateString();
                let time_today = new Date().toLocaleTimeString([], {hour: '2-digit', minute: '2-digit'})
                download.href = window.URL.createObjectURL(data);
                download.download = "CME_" + date_today + "_" + time_today;
                download.click();
                loadingIcon.style.display = "none";
            }).catch(() => {
            })
        }
    }

    _filterResultsChallengeList(filteredWord) {
        this.filteredList = [];
        if (!(filteredWord === "")) {
            for (let challenge of this.challengeList) {
                if (challenge.name.toLowerCase().includes(filteredWord.toLowerCase()) || challenge.category.toLowerCase().includes(filteredWord.toLowerCase()) || challenge.type.toLowerCase().includes(filteredWord.toLowerCase())) {
                    this.filteredList.push(challenge);
                }
            }
        } else {
            this.filteredList = this.challengeList;
        }
    }
}

customElements.define('application-component', ApplicationComponent);
