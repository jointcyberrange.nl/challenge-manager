import {css, html, LitElement} from 'https://unpkg.com/lit-element/lit-element.js?module';

class ChallengeComponent extends LitElement {

    static get properties() {
        return {
            infoModalOpen: {type: Boolean},
            challengeData: {type: JSON},
            isChecked: {type: Boolean}
        }
    }

    static get styles() {
        return css`
          .challenge-wrapper {
            display: flex;
            flex-direction: column;
            color: black;
            text-align: left;
            background: white;
            border-bottom-left-radius: 5px;
            border-top-left-radius: 5px;
            border-bottom-right-radius: 5px;
            padding: 15px;
            width: 25rem;
            cursor: pointer;
          }

          .name {
            color: rgba(237, 37, 78, 100);
            font-weight: bold;
            font-family: Conthrax, sans-serif;
          }
          
          .challengeAndCheckbox {
            display: flex;
            margin: 1.5rem;
            background-color: white;
            border-radius: 5px;
          }
          
          .challengeCheckbox {
            display: block;
            position: relative;
            cursor: pointer;
            font-size: 16px;
          }

          /* I got most of the code underneath this comment from W3Schools (adapted it a bit to my own liking), */
          /* but a citation still felt needed */

          /* How To Create a Custom Checkbox and Radio Buttons. (z.d.). W3Schools. */
          /* Retrieved 18 juni 2022, */
          /* from https://www.w3schools.com/howto/howto_css_custom_checkbox.asp */

          .challengeCheckbox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
          }

          .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
          }

          .challengeCheckbox:hover input ~ .checkmark {
            background-color: #ccc;
          }

          .challengeCheckbox input:checked ~ .checkmark {
            background-color: rgba(237, 37, 78, 100);
          }

          .checkmark:after {
            content: "";
            position: absolute;
            display: none;
          }

          .challengeCheckbox input:checked ~ .checkmark:after {
            display: block;
          }

          .challengeCheckbox .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            transform: rotate(45deg);
          }
        `;
    }

    render() {
        return html`
            <div class="challengeAndCheckbox">
                <div class="challenge-wrapper">
                    <div class="name">${this.challengeData.name}</div>
                    <div class="category">${this.challengeData.category}</div>
                    <div class="type">${this.challengeData.type}</div>
                </div>
                <label class="challengeCheckbox">
                    <input class="challengeCheckboxInput" type="checkbox" >
                    <span class="checkmark"></span>
                </label>
            </div>
            <challenge-popup .open="${this.infoModalOpen}" .challengeData="${this.challengeData}"></challenge-popup>
        `;
    }

    connectedCallback() {
        super.connectedCallback();
        this.infoModalOpen = false;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties)

        if (this.isChecked === undefined) this.isChecked = false;
        else this.shadowRoot.querySelector(".challengeCheckboxInput").checked = this.isChecked;

        this.shadowRoot.addEventListener("modalClosed", () => {
            this.infoModalOpen = false;
        })

        this.shadowRoot.querySelector(".challenge-wrapper").addEventListener("click", () => {
            this.infoModalOpen = true;
        })

        const checkbox = this.shadowRoot.querySelector(".challengeCheckboxInput");
        checkbox.addEventListener("click", () => {
            if (checkbox.checked) {
                this.isChecked = true;
                this.isCheckedEvent = new CustomEvent("isCheckedEvent", {
                    bubbles: true,
                    composed: true,
                    cancelable: false,
                    detail: this.challengeData
                });
                this.dispatchEvent(this.isCheckedEvent);
            } else if (!checkbox.checked) {
                this.isChecked = false;
                this.isUncheckedEvent = new CustomEvent("isUncheckedEvent", {
                    bubbles: true,
                    composed: true,
                    cancelable: false,
                    detail: this.challengeData
                });
                this.dispatchEvent(this.isUncheckedEvent);
            }
        })
    }
}

customElements.define('challenge-component', ChallengeComponent);
