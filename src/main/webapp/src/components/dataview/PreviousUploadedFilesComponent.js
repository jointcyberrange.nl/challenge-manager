import {css, html, LitElement} from 'https://unpkg.com/lit-element/lit-element.js?module';

class PreviousUploadedFilesComponent extends LitElement {

    static get properties() {
        return {
            previouslyUploaded: {type: Array}
        }
    }

    static get styles() {
        return css`
            .previously-uploaded-div {
                position: absolute;
                top: 13.5rem;
                left: 0.5rem;
            }
            
            .files-list {
                background-color: white;
                color: black;
                width: 9.5rem;
                border-radius: 0.5rem;
                padding-left: 1.5rem;
                width: 10.5rem;
            }
            
            .label-and-info {
                display: flex;
                justify-content: space-around;
            }

            /* Tooltip container */
            .tooltip {
                position: relative;
                display: inline-block;
                border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
            }

            /* Tooltip text */
            .tooltip .tooltiptext {
                visibility: hidden;
                width: 12rem;
                background-color: gray;
                opacity: 0.9;
                color: #fff;
                text-align: center;
                padding: 5px 0;
                border-radius: 6px;

                /* Position the tooltip text - see examples below! */
                position: absolute;
                z-index: 1;

                bottom: 100%;
                left: 40%;
                margin-left: -175px; /* Use half of the width (120/2 = 60), to center the tooltip */
                margin-bottom: 0.5rem;
            }

            /* Show the tooltip text when you mouse over the tooltip container */
            .tooltip:hover .tooltiptext {
                visibility: visible;
            }

            .tooltip .tooltiptext::after {
                content: " ";
                position: absolute;
                top: 100%; /* At the bottom of the tooltip */
                right: 6%;
                margin-left: -5px;
                border-width: 5px;
                border-style: solid;
                border-color: gray transparent transparent transparent;
                opacity: 0.9;
            }
        `;
    }

    render() {
        return html`
            <div class="previously-uploaded-div">
                <div class="label-and-info">
                    <label for="files-list-div" class="previous-files-label">Previously uploaded</label>
                    <div class="tooltip">&#9432;
                        <span class="tooltiptext">The last imported CTFD environment is the one that you will be exporting the new list of challenges into.</span>
                    </div>
                </div>
                <div id="files-list-div">
                    <ul class="files-list"></ul>
                </div>
            </div>
        `;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
    }

    updateList() {
        let updatedListFileNames = this.previouslyUploaded;
        let filesList = this.shadowRoot.querySelector(".files-list")
        filesList.innerHTML = "";
        for (let i = 0; i < updatedListFileNames.length; i++) {
            let fileName = document.createElement("li");
            fileName.textContent = updatedListFileNames[i];
            filesList.appendChild(fileName);
        }
    }
}

customElements.define('previous-files', PreviousUploadedFilesComponent);
