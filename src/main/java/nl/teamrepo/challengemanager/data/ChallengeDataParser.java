package nl.teamrepo.challengemanager.data;

import nl.teamrepo.challengemanager.domain.Challenge;
import nl.teamrepo.challengemanager.domain.enums.ChallengeState;
import nl.teamrepo.challengemanager.domain.exceptions.ExportParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.boot.json.JsonParseException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.rmi.server.ExportException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ChallengeDataParser {

    public static ArrayList<Challenge> parseJsonChallengesFromArray(JSONArray challengeArray) {
        return createChallengeFromJSONArray(challengeArray);
    }

    // Overloading, same method takes as parameter either a zipfile or the path to one.
    public static List<Challenge> extractCTFDExportZipToChallengeList(ZipFile CTFDExportZip) throws IOException, ParseException, ExportParseException {
        List<Challenge> challenges = new ArrayList<>();

        return getChallengesFromZip(challenges, CTFDExportZip);
    }

    public static List<Challenge> extractCTFDExportZipToChallengeList(String path) throws IOException, ParseException, ExportParseException {
        List<Challenge> challenges = new ArrayList<>();

        ZipFile CTFDExportZip = new ZipFile(path);
        return getChallengesFromZip(challenges, CTFDExportZip);
    }

    public static ArrayList<Challenge> createChallengeFromJSONArray(JSONArray challengeArray) {
        ArrayList<Challenge> challenges = new ArrayList<>();

        for (Object object : challengeArray) {
            JSONObject result = (JSONObject) object;
            long id = (long) result.get("id");
            String name = (String) result.get("name");
            String description = (String) result.get("description");
            int maxAttempts = Integer.parseInt(result.get("max_attempts").toString());
            int value = Integer.parseInt(result.get("value").toString());
            String category = (String) result.get("category");
            String type = (String) result.get("type");

            ChallengeState state;
            try {
                state = ChallengeState.valueOf(result.get("state").toString().toUpperCase(Locale.ROOT));
            } catch (Exception e) {
                state = ChallengeState.HIDDEN;
            }

            String requirements = (String) result.get("requirements");
            String connectionInfo = (String) result.get("connection_info");

            Challenge challenge = new Challenge(id, name, description, maxAttempts, value, category, type, state, requirements, connectionInfo);
            challenges.add(challenge);
        }

        return challenges;
    }

    public static ArrayList<Challenge> parseJsonChallengesFromPath(String path) {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(path)) {
            JSONObject obj = (JSONObject) jsonParser.parse(reader);
            JSONArray results = (JSONArray) obj.get("results");

            return createChallengeFromJSONArray(results);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    private static List<Challenge> getChallengesFromZip(List<Challenge> challenges, ZipFile CTFDExportZip) throws IOException, ParseException, ExportParseException {
        if (CTFDExportZip == null )return new ArrayList<>();

        try {
            for (Enumeration<? extends ZipEntry> e = CTFDExportZip.entries(); e.hasMoreElements(); ) {
                ZipEntry entry = e.nextElement();

                if (!entry.isDirectory() && entry.getName().equals("db/challenges.json")) {
                    InputStream stream = CTFDExportZip.getInputStream(entry);
                    JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(stream, StandardCharsets.UTF_8));
                    JSONArray challengeJSONArray = (JSONArray) jsonObject.get("results");

                    challenges = ChallengeDataParser.parseJsonChallengesFromArray(challengeJSONArray);
                    stream.close();
                }
            }
        } catch(Exception e) {
            throw new ExportParseException();
        }
        CTFDExportZip.stream().close();
        CTFDExportZip.close();
        return challenges;
    }
}
