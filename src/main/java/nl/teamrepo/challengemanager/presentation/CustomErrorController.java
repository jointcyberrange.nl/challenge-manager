package nl.teamrepo.challengemanager.presentation;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Most of this code comes from a web tutorial.
 * B. (2022b, June 4). Spring Boot: Customize Whitelabel Error Page. Baeldung.
 * Retrieved June 25, 2022, from https://www.baeldung.com/spring-boot-custom-error-page
 */
@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "error-404.html";
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return "error-403.html";
            }
        }

        return "error.html";
    }

    @Bean
    public String getErrorPath() {
        return "/error";
    }
}