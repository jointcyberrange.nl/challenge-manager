package nl.teamrepo.challengemanager.domain.exceptions;

public class ExportParseException extends Exception {
    public ExportParseException() {
        super("Can't parse this CTFD Export.");
    }
}
