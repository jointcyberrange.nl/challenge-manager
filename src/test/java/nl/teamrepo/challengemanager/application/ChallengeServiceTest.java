package nl.teamrepo.challengemanager.application;

import nl.teamrepo.challengemanager.domain.Challenge;
import nl.teamrepo.challengemanager.domain.enums.ChallengeState;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ChallengeServiceTest {
    Challenge challenge1;
    ChallengeService challengeService;

    @BeforeAll
    void init(){
        challenge1 = new Challenge(
                1,
                "Challenge",
                "Test",
                1,
                10,
                "CSC",
                "Standard",
                ChallengeState.VISIBLE,
                null,
                null);
        List<Challenge> challengeList = new ArrayList<>();
        challengeList.add(challenge1);
        challengeService = new ChallengeService();
    }

//    @Test
//    void findById() {
//        assertEquals(challenge1,challengeService.findChallengeById(1));
//    }
//
//    @Test
//    void getChallenge(){
//        List<Challenge> challengeList = new ArrayList<>();
//        challengeList.add(challenge1);
//        assertEquals(challengeList, challengeService.getCurrentExportZipChallenges());
//    }
//
//    @Test
//    void getName(){
//        assertEquals(challenge1.getName(), challengeService.findChallengeById(1).getName());
//    }
//
//    @Test
//    void getDescription(){
//        assertEquals(challenge1.getDescription(), challengeService.findChallengeById(1).getDescription());
//    }
//
//    @Test
//    void getMaxAttempts(){
//        assertEquals(challenge1.getMax_attempts(), challengeService.findChallengeById(1).getMax_attempts());
//    }
//
//    @Test
//    void getValue(){
//        assertEquals(challenge1.getValue(), challengeService.findChallengeById(1).getValue());
//    }
//
//    @Test
//    void getCatagory(){
//        assertEquals(challenge1.getCategory(), challengeService.findChallengeById(1).getCategory());
//    }
//
//    @Test
//    void getType(){
//        assertEquals(challenge1.getType(), challengeService.findChallengeById(1).getType());
//    }
//
//    @Test
//    void getChallengeState(){
//        assertEquals(challenge1.getState(), challengeService.findChallengeById(1).getState());
//    }
//
//    @Test
//    void getRequirements(){
//        assertEquals(challenge1.getRequirements(), challengeService.findChallengeById(1).getRequirements());
//    }
//
//    @Test
//    void getConnectionInfo(){
//        assertEquals(challenge1.getConnectionInfo(), challengeService.findChallengeById(1).getConnectionInfo());
//    }
//
//    @Test
//    void unknownIDtest(){
//        assertNotEquals(challenge1, challengeService.findChallengeById(2));
//    }

}