package nl.teamrepo.challengemanager.domain;

import nl.teamrepo.challengemanager.domain.enums.ChallengeState;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChallengeTest {
    private Challenge challenge1;

    @BeforeEach
    public void init(){
        challenge1 = new Challenge(
                1,
                "Challenge",
                "Test",
                1,
                10,
                "CSC",
                "Standard",
                ChallengeState.VISIBLE,
                null,
                null);
    }

    @Test
    public void challengeToString(){
        assertEquals(("Id: 1, name: Challenge\n" +
                "Description: \n" +
                "-------\n" +
                "Test\n" +
                "---------\n" +
                "Max attempts: 1\n" +
                "Value: 10\n" +
                "Category: CSC\n" +
                "Type: Standard\n" +
                "State: visible\n" +
                "Requirements: null\n" +
                "Connection info: null"), challenge1.toString());
    }
}