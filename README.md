# Challenge Manager
The goal of this project is to provide an easy-to-use website where challenges can be more easily setup and selected for
CTFD Exports/environments.  A beta version will soon be hosted on heroku. 

This repository also contains a small cli program, the console application, 
so you can try out some basic functionality that the server will provide for the Challenge Manager website.
___

## How to use the website
**This webapp has been built for teachers who use the CTF challenges provided by Joint Cyber Range.**

1.	Click the _“Import”_ button

&emsp; <img src="docs/images/import.png" alt="import.png" width="150"/>

2.	Choose the desired CTFD environment

	2.2 Click _“Open”_ 

3.	The CTF challenges are now able to be viewed or selected

 	3.1 Decide if you would like to import more CTFD environments
   
 	&emsp;3.1.1 Would you like to add another CTFD environment? Repeat from step 1

 	&emsp;3.1.2 Would you like to overwrite the previously uploaded CTFD environment? Repeat from step 1 with the _“Overwrite previous challenges”_ checkbox checked

  &emsp; <img src="docs/images/overwrite.png" alt="overwrite.png" width="250"/>

4.	If you would like to filter through the list of CTF challenges, click on the _“Search”_ bar and type in any variables you would like to filter on

&emsp; <img src="docs/images/search.png" alt="search.png" width="300"/>

  &emsp; &nbsp; &nbsp;4.1 Would you like to just view challenges? Proceed to step 5.

  &emsp; &nbsp; &nbsp;4.2 Would you like to update the list of CTF challenges in your CTFD environment? Proceed to step 6

5.	Click on a CTF challenge to view its information

&emsp; <img src="docs/images/info.png" alt="info.png" width="300"/>

6.	Select the desired CTF challenges

&emsp; <img src="docs/images/exampleChallenge.png" alt="exampleChallenge.png" width="300"/>

7.	Choose if you allow your personal data that is stored inside the CTFD environment to be exported

 	7.1 Yes? Proceed to step 8

 	7.2 No? Check the checkbox _“Remove personal data”_ and proceed to step 8

&emsp; <img src="docs/images/personalData.png" alt="personalData.png" width="250"/>

8.	Click the _“Export”_ button to download your updated CTFD environment with an updated list of challenges

&emsp; <img src="docs/images/export.png" alt="export.png" width="150"/>

9.	Are you done and would you to clear the screen and currently loaded exports? Click the button _“Clear Session”_

&emsp; <img src="docs/images/clearSession.png" alt="clearSession.png" width="150"/>

___

## How to run locally in docker

### Requirements:

Docker (desktop) and WSL installed:
1.  [Install and setup Docker Desktop](https://gitlab.com/jointcyberrange.nl/getting-started#12-install-and-setup-docker-desktop)

### Manual
1.  Start docker and open WSL
2. Run the following command to download and run the container image:
    ````
    docker run -d -p 8080:8080 registry.gitlab.com/jointcyberrange.nl/challenge-manager:latest
    ````
3.  You can now visit the challenge manager at http://localhost:8080

---

## How to deploy

### Requirements: 
Git and Heroku CLI installed
1.	https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
2.	https://devcenter.heroku.com/articles/heroku-cli#install-the-heroku-cli

### Manual:
1.	Create an app in Heroku
2.	Navigate to the IDE of your choice
3.	Clone the project into your IDE 
 	https://gitlab.com/jointcyberrange.nl/challenge-manager.git
4.	Change the `BASE_URL` variable in `ApplicationComponent.js` to 
 	`https://[your app name here].herokuapp.com`
5.	Add a remote to your local repository with the `heroku git:remote` command. Enter your app’s name instead of _“example-app”_ <sup>1</sup>:

&emsp; <img src="docs/images/remote.png" alt="remote.png" width="300"/>
 
6.	Push the project to your app, using the git push command. This is to specifically push the main branch <sup>1</sup>:

&emsp; <img src="docs/images/push.png" alt="push.png" width="210"/>

**If you would like to create a staging and production pipeline on Heroku, follow the steps in [this](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4) manual.**
> **_NOTE:_**  Please also take the following notes into consideration:
-	Start following the steps from _“Setting up Heroku”_.
-	Ignore anything that has to do with Node and NPM.
-	You can use the `gitlab-ci.yml` file from the project repository.
-	Follow the steps until the end.

### References:
<sup>1</sup> Heroku. (z.d.). Deploying with Git | Heroku Dev Center. Heroku Dev Center. 
  consulted on 2 juli 2022, from https://devcenter.heroku.com/articles/git
___

## How to run the Console application
To run the console application, you can follow the following steps:
   - Download the *ChallengeManagerBE.jar*
   - To run this application:
        - Find the path to the *ChallengeManagerBE.jar* and open a terminal. 
        - Write `java -jar` and paste the path you just copied.
        - If you don't provide the path to an CTFD Export zip file, the app will create dummy data for you:
   
          `java -jar path/to/ChallengeManagerBE.jar`
        - Otherwise, the command will look something like this:
          `java -jar path/to/ChallengeManagerBE.jar path/to/CTFDzipfile.zip`

### Important:
The console application can only run if java version 11 or higher is installed on your computer.

If the application does not start, it might also be necessary to manually configure this in your environment variables path.

For installing java 11 or higher see:
https://www.oracle.com/java/technologies/downloads/#java11

___
## Authors: Team Repo
#### Members:
 - Husyin Akgün
 - Damian Brands
 - Stije Hardeman
 - Joy Krabbe
 - Sophie van der Most
