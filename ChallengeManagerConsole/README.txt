To start the application, click the LaunchApplication.bat.
This program can only run if java version 11 is installed on your computer.

If the application does not start, it might also be necessary to manually 
configure this in your path.

For installing java 11 see:
https://www.oracle.com/java/technologies/downloads/#java11